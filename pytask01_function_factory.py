"""First task creation of function factory"""
import operator
from functools import partial


def add_factory1(x):
    """First approach
    Takes number and returns function"""
    if x == 1:
        return func11
    elif x == 2:
        return func12
    elif x == 3:
        return func13


def func11(y):
    """Function returns square root of number"""
    print y ** 0.5


def func12(y):
    """Function returns square of number"""
    print y ** 2


def func13(y):
    """Function returns cube of number"""
    print y ** 3

add5 = add_factory1(2)
add5(100)


def add_factory2(x):
    """Second approach
        Function closure"""
    if x == 1:
        def func21(y):
            """Function returns square root of number"""
            print y ** 0.5
        return func21
    elif x == 2:
        def func22(y):
            """Function returns square of number"""
            print y ** 2
        return func22
    elif x == 3:
        def func23(y):
            """Function returns cube of number"""
            print y ** 3
        return func23

add5 = add_factory2(3)
add5(100)


def add_factory3(x):
    """Third approach
    Operator import"""
    if operator.eq(x, 1):
        return func31
    if operator.eq(x, 2):
        return func32
    if operator.eq(x, 3):
        return func33


def func31(y):
    """Function returns square root of number"""
    print y ** 0.5


def func32(y):
    """Function returns square of number"""
    print y ** 2


def func33(y):
    """Function returns cube of number"""
    print y ** 3

add5 = add_factory3(1)
add5(100)


def add_factory4(x):
    """Fourth approach
    Operator import"""
    if operator.__eq__(x, 1):
        return func41
    elif operator.__eq__(x, 2):
        return func42
    elif operator.__eq__(x, 3):
        return func43


def func41(y):
    """Function returns square root of number"""
    print y ** 0.5


def func42(y):
    """Function returns square of number"""
    print y ** 2


def func43(y):
    """Function returns cube of number"""
    print y ** 3

add5 = add_factory4(2)
add5(100)


def add_factory5(x):
    """Fifth approach"""
    if x == 1:
        func = partial(func51)
    elif x == 2:
        func = partial(func52)
    elif x == 3:
        func = partial(func53)
    return func


def func51(y):
    """Function returns square root of number"""
    print y ** 0.5


def func52(y):
    """Function returns square of number"""
    print y ** 2


def func53(y):
    """Function returns cube of number"""
    print y ** 3


add5 = add_factory5(2)
add5(10)
