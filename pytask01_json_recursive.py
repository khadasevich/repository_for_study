import urllib
import json


def url_open(u):
    """Function takes url and operate json"""
    response = urllib.urlopen(u)
    dat = response.read()
    json_dict = json.loads(dat)
    for key, value in json_dict.iteritems():
        if isinstance(value, int):
            print " Repeat script later "
    def rec_dict(x):
        """Function takes json from page and returns all titles"""
        m = []
        for key, value in x.iteritems():
            if key == "title":
                m.append(value)
            elif isinstance(value, dict):
                m.extend(rec_dict(value))
            elif isinstance(value, list):
                for element in value:
                    if isinstance(element, dict):
                        m.extend(rec_dict(element))
        return m
    v = rec_dict(json_dict)
    # return v[15]
    # for element in v:
        # print element
    def gen_ob(x):
        for element in x:
            yield element
            # n.next()
    return gen_ob(v)

url = "http://www.reddit.com/r/python.json"
print url_open(url)

# gen_ob = rec_dict(json_dict)
# print gen_ob

